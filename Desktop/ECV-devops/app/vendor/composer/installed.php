<?php return array(
    'root' => array(
        'pretty_version' => 'dev-clemence',
        'version' => 'dev-clemence',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '22fdc206a2834b91025bcfe28e99ef8e47091a79',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-clemence',
            'version' => 'dev-clemence',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '22fdc206a2834b91025bcfe28e99ef8e47091a79',
            'dev_requirement' => false,
        ),
        'ehime/hello-world' => array(
            'pretty_version' => '1.0.5',
            'version' => '1.0.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ehime/hello-world',
            'aliases' => array(),
            'reference' => 'b1c8cdd2c11272d8c5deec7816e51fa5374217c1',
            'dev_requirement' => false,
        ),
    ),
);
